"use strict";
import chai from "chai";
import {LogField, Bin, BinStream} from "ardubinreader";
import {contents} from "./examplefile.js";
import fs from 'fs';
//let Buffer = NewBuffer;

let expect = chai.expect
let should = chai.should()

const contents2 = fs.readFileSync('examples/00000030.BIN');

describe("Test reading data from a bin stream", function() {
    
  let testBinStream = new BinStream(contents);

  it('confirm the binreader has been initialised correctly', function() {

    let bin = new BinStream(new Uint8Array([1, 2, 3, 4, 5, 6]))

    expect(bin.get_index()).to.be.equal(0);
    expect(bin.get_length()).to.be.equal(6);
  })

  it('should read a fixed number of bytes', function() {
    let bin = new BinStream(new Uint8Array([1, 2, 3, 4, 5, 6]))
    let temp = bin.read(2);
    expect(temp[0]).to.be.equal(1);
    expect(temp[1]).to.be.equal(2);
    expect(temp.length).to.be.equal(2);
    let temp2 = bin.read(4);
    expect(temp2[0]).to.be.equal(3);
    expect(temp2[1]).to.be.equal(4);
    expect(temp2.length).to.be.equal(4);
    expect( bin.get_eof()).to.be.true;

  })

  it('should get the index of a separator', function () {
    let bin = new BinStream(new Uint8Array([1, 2, 3, 163, 149, 6]));
    expect(bin.indexOf(new Uint8Array([163, 149]))).to.be.equal(3);

    expect(bin.indexOf(new Uint8Array([163, 6]))).to.be.equal(-1);
    expect(bin.indexOf(new Uint8Array([1, 2]))).to.be.equal(0);
    expect(bin.indexOf(new Uint8Array([163, 149]),4)).to.be.equal(-1);

  })

  it('should read until the line break', function () {
    
    let bin = new BinStream(new Uint8Array([1, 2, 3, 163, 149, 6]))
    
    const searchval = new Uint8Array([163, 149]);
    const a_line = bin.read_to(searchval);

    expect(a_line.length).to.be.equal(3);

    expect(bin.get_index()).to.be.equal(5);
    
  })

  it('should jump around a bin stream', function() {
    const tempbin = new BinStream(new Uint8Array([1, 2, 3, 4, 5, 6, 7, 8]));

    tempbin.jump_to(new Uint8Array([5]));
    expect(tempbin.get_index()).to.be.equal(5);

    tempbin.jump_to(new Uint8Array([7]));
    expect(tempbin.get_index()).to.be.equal(7);
  })

})


describe("test decoding stuff from a bin stream", function() {

  it('should read an int8 correctly', function() {
    const buf = new Uint8Array(new Int8Array([2, -2]).buffer);
    const bintemp = new BinStream(buf);
    expect(bintemp.decode_method['b']()).to.be.equal(2);
    expect(bintemp.decode_method['b']()).to.be.equal(-2);
    expect(bintemp.get_eof()).to.be.true;
  })

  it('should read a uint8 correctly', function() {
    const bintemp = new BinStream(new Uint8Array([2, 4]));
    expect(bintemp.decode_method['b']()).to.be.equal(2);
    expect(bintemp.decode_method['b']()).to.be.equal(4);
    expect(bintemp.get_eof()).to.be.true;
  })

  it('should read a Int16 correctly', function() {
    const buf = new Uint8Array(new Int16Array([32767, -32768]).buffer);
    const bintemp = new BinStream(buf);

    expect(bintemp.decode_method['h']()).to.be.equal(32767);
    expect(bintemp.decode_method['h']()).to.be.equal(-32768);
    expect(bintemp.get_eof()).to.be.true;
  })

  it('should read a UInt16 correctly', function() {
    const buf = new Uint8Array(new Uint16Array([32767, 65535]).buffer);
    const bintemp = new BinStream(buf);

    expect(bintemp.decode_method['H']()).to.be.equal(32767);
    expect(bintemp.decode_method['H']()).to.be.equal(65535);
    expect(bintemp.get_eof()).to.be.true;
  })


  it('should read a UInt32 correctly', function() {
    const buf = new Uint8Array(new Uint32Array([4294967290, 4294967294]).buffer);
    const bintemp = new BinStream(buf);
    expect(bintemp.decode_method['I']()).to.be.equal(4294967290);
    expect(bintemp.get_index()).to.be.equal(4);
    expect(bintemp.decode_method['I']()).to.be.equal(4294967294);
    
    expect(bintemp.get_eof()).to.be.true;
  })

  it('should read a Int32 correctly', function() {
    const buf = new Uint8Array(new Int32Array([2147483647, 20]).buffer);
    const bintemp = new BinStream(buf);
    expect(bintemp.decode_method['i']()).to.be.equal(2147483647);
    expect(bintemp.get_index()).to.be.equal(4);
    expect(bintemp.decode_method['i']()).to.be.equal(20);
    
    expect(bintemp.get_eof()).to.be.true;
  })

  it('should read a Float32 correctly', function() {
    const buf = new Uint8Array(new Float32Array([1.23456, 7.6543521]).buffer);
    const bintemp = new BinStream(buf);
    expect(bintemp.decode_method['f']()).to.be.approximately(1.23456, 0.0000001);
    expect(bintemp.get_index()).to.be.equal(4);
    expect(bintemp.decode_method['f']()).to.be.approximately(7.6543521, 0.0000001);
    
    expect(bintemp.get_eof()).to.be.true;
  })

  it('should read a Double correctly', function() {
    const buf = new Uint8Array(new Float64Array([1.234567891011, 7.65435215351]).buffer);
    const bintemp = new BinStream(buf);
    expect(bintemp.decode_method['d']()).to.be.approximately(1.234567891011, 0.000000000001);
    expect(bintemp.get_index()).to.be.equal(8);
    expect(bintemp.decode_method['d']()).to.be.approximately(7.65435215351, 0.0000000000001);
    
    expect(bintemp.get_eof()).to.be.true;
  })


  it('should read an Int16[32] correctly', function() {
    const arr =[];
    for (let i=0; i<16; i++) {
      arr.push(1234);
      arr.push(-1234);
    }
    const buf = new Uint8Array(new Int16Array(arr).buffer);

    const bintemp = new BinStream(buf);

    let binarr = bintemp.decode_method['a']();
    expect(binarr[0]).to.be.equal(1234);
    expect(binarr[7]).to.be.equal(-1234);
    expect(bintemp.get_eof()).to.be.true;
  })


  it('should read some chars correctly', function() {

    const buf = Buffer.from('abcdefgh', 'latin1');

    const bintemp = new BinStream(buf);
    expect(bintemp.decode_method['n']()).to.be.equal('abcd');
    expect(bintemp.decode_method['n']()).to.be.equal('efgh');
    expect(bintemp.get_eof()).to.be.true;
  })

  it('should read a sequence of two char 4s correctly', function() {

    const buf = Buffer.from('abcdefgh', 'latin1');

    const bintemp = new BinStream(buf);
    const res = bintemp.read_encoding('nn')
    expect(res[0]).to.be.equal('abcd');
    expect(res[1]).to.be.equal('efgh');
    expect(bintemp.get_eof()).to.be.true;
  })

})




describe('test the  LogField class', () => {
  it('should update data based on mults', function(){
    const logf = new LogField(
      'GPS', 
      '',
      'TimeUS,Status,GMS,GWk,NSats,HDop,Lat,Lng,Alt,Spd,GCrs,VZ,Yaw,U'
      )
      logf.mults=[9.999999974752427e-7,0,0,0,1,0.009999999776482582,1.0000000116860974e-7,1.0000000116860974e-7,0.009999999776482582,1,1,1,0,0];
      const vals = logf.applymults([
        1,1,1,1,1,1,1,1,1,1,1,1,1,1
      ]);
      expect(vals[0]).to.be.equal(9.999999974752427e-7);
      expect(vals[1]).to.be.equal(1);
      expect(vals[13]).to.be.equal(1);
      expect(vals[14]).to.be.undefined;
  })

})

describe("test reading a bin file", function() {

  it('should read the format definition when nothing else is available', function() {
    const bin = new Bin(contents);
    bin._stream.jump_to(new Uint8Array([163, 149, 128]));
    const fmtline = bin._fields[128].makedict(bin._read_field(bin._fields[128])) ;

    expect(fmtline.Type).to.be.equal(128);
    expect(fmtline.Length).to.be.equal(89);
    expect(fmtline.Name).to.be.equal('FMT');
    expect(fmtline.Format).to.be.equal('BBnNZ');
    expect(fmtline.Columns).to.be.equal('Type,Length,Name,Format,Columns');

  })

  it('should create a field', function() {
    const bin = new Bin(contents);
    bin._stream.jump_to(new Uint8Array([163, 149, 128]));
    const fmtline = bin._read_field(bin._fields[128]);
    bin._stream.jump_to(new Uint8Array([163, 149, 128]));
    bin._create_field(bin._read_field(bin._fields[128]), 128);
    expect(bin._fields[156].name).to.be.equal('UNIT');
    expect(bin._fields[156].encoding).to.be.equal('QbZ');
    expect(bin._fields[156].labels[0]).to.be.equal('TimeUS');
    expect(bin._fields[156].labels[1]).to.be.equal('Id');
    expect(bin._fields[156].labels[2]).to.be.equal('Label');
  })

  it('should create 10 fields', function() {
    const bin = new Bin(contents);
    for (let i=0; i<10; i++) {bin._parse_line();}
    expect(Object.keys(bin._fields).length).to.be.equal(10);

  })

  it('should return a special parser', function() {
    const bin = new Bin(contents);
    const line1 = bin._fields[128].makedict(bin._parse_line());

    expect(line1.Type).to.be.equal(128);
    expect(line1.Name).to.be.equal('FMT');
    expect(bin._special_fields[line1.Name]).to.be.equal(bin._special_fields['FMT']);
    expect(line1.Name in bin._special_fields).to.be.true;
  } )

  it('should add mults to a field', function() {
    const bin = new Bin(contents);
    for (let i=0; i<2000; i++) {bin._parse_line();}
    
    console.log(bin._fields[151].mults);
    expect(bin._fields[151].mults[0]).to.be.closeTo(1e-6, 1e-8)
    expect(bin._fields[151].mults[4]).to.be.closeTo(1e-2, 1e-8);
    expect(bin._fields[151].mults[7]).to.be.closeTo(1e-3, 1e-8);
  })


  it('should get the lat long in degrees', function() {
    const bin2 = new Bin(contents2);
    bin2.parse_all();
    const gpsdat = bin2._data['GPS'];
    expect(gpsdat[1000][6]).to.be.closeTo(51.4582792,4);

  }) 





})


