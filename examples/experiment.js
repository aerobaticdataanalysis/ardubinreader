"use strict"

import fs from 'fs';
import {Bin} from 'ardubinreader';

const buf = fs.readFileSync('./examples/00000030.BIN');
const bin = new Bin(buf);

bin.parse_all();

console.log(Object.keys(bin._data));

