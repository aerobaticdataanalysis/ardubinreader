"use strict";

require("core-js/modules/es.array-buffer.slice");

require("core-js/modules/es.string.split");

require("core-js/modules/es.typed-array.float32-array");

require("core-js/modules/es.typed-array.float64-array");

require("core-js/modules/es.typed-array.uint8-array");

require("core-js/modules/es.typed-array.uint32-array");

require("core-js/modules/es.typed-array.to-locale-string");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Bin = exports.LogField = exports.BinStream = void 0;

function readUInt8(u8arr, index) {
  return u8arr[index];
}

function readInt8(u8arr, index) {
  return u8arr[index] << 24 >> 24;
}

function readUInt16LE(u8arr, index) {
  return u8arr[index + 1] << 8 | u8arr[index];
}

function readInt16LE(u8arr, index) {
  return readUInt16LE(u8arr, index) << 16 >> 16;
}

function readUInt32LE(u8arr, index) {
  return new Uint32Array(new Uint8Array([u8arr[index], u8arr[index + 1], u8arr[index + 2], u8arr[index + 3]]).buffer)[0];
}

function readInt32LE(u8arr, index) {
  return readUInt32LE(u8arr, index) << 32 >> 32;
}

function readFloatLE(u8arr, index) {
  return new Float32Array(new Uint8Array([u8arr[index], u8arr[index + 1], u8arr[index + 2], u8arr[index + 3]]).buffer)[0];
}

function readDoubleLE(u8arr, index) {
  return new Float64Array(new Uint8Array([u8arr[index], u8arr[index + 1], u8arr[index + 2], u8arr[index + 3], u8arr[index + 4], u8arr[index + 5], u8arr[index + 6], u8arr[index + 7]]).buffer)[0];
}

function readBigUInt64LE(u8arr, index) {
  return new BigInt64Array(new Uint8Array([u8arr[index], u8arr[index + 1], u8arr[index + 2], u8arr[index + 3], u8arr[index + 4], u8arr[index + 5], u8arr[index + 6], u8arr[index + 7]]).buffer)[0];
}

function readBigInt64LE(u8arr, index) {
  return readBigUInt64LE(u8arr, index) << 64 >> 64;
}

function readChar4(u8arr, index) {
  return String.fromCharCode.apply(null, u8arr.subarray(index, index + 4)).split('\u0000')[0];
}

function readChar16(u8arr, index) {
  return String.fromCharCode.apply(null, u8arr.subarray(index, index + 16)).split('\u0000')[0];
}

function readChar64(u8arr, index) {
  return String.fromCharCode.apply(null, u8arr.subarray(index, index + 64)).split('\u0000')[0];
}

class BinStream {
  constructor(buf) {
    this._buffer = buf; // this is going to be a Uint8Array

    this._index = 0;
    this._length = this._buffer.byteLength;
    this._eof = false;
    this.get_index.bind(this);
    this.get_length.bind(this);
    this.read.bind(this);
    this.get_remaining.bind(this);
    this.read_to.bind(this);
    this.jump_to.bind(this); //const val = new Uint8Array(reader.result);
    //console.log(String.fromCharCode(val[7]));

    this.decode_method = {
      'a': () => {
        let arr = [];

        for (let i = 0; i < 32; i++) {
          arr.push(this.read_func(readInt16LE, 2));
        }

        return arr;
      },
      "b": () => this.read_func(readInt8, 1),
      "B": () => this.read_func(readUInt8, 1),
      "h": () => this.read_func(readInt16LE, 2),
      "H": () => this.read_func(readUInt16LE, 2),
      "i": () => this.read_func(readInt32LE, 4),
      "I": () => this.read_func(readUInt32LE, 4),
      "f": () => this.read_func(readFloatLE, 4),
      "d": () => this.read_func(readDoubleLE, 8),
      "n": () => this.read_func(readChar4, 4),
      "N": () => this.read_func(readChar16, 16),
      "Z": () => this.read_func(readChar64, 64),
      "c": () => this.read_func(readInt16LE, 2) / 100,
      "C": () => this.read_func(readUInt16LE, 2) / 100,
      "e": () => this.read_func(readInt32LE, 4) / 100,
      "E": () => this.read_func(readUInt32LE, 4) / 100,
      "L": () => this.read_func(readInt32LE, 4),
      "M": () => this.read_func(readUInt8, 1),
      "q": () => parseInt(this.read_func(readBigInt64LE, 8)),
      "Q": () => parseInt(this.read_func(readBigUInt64LE, 8))
    };
    this.read_encoding.bind(this);
  }

  get_eof() {
    return this._eof;
  }

  get_index() {
    return this._index;
  }

  set_index(value) {
    if (value >= this._length) {
      this._eof = true;
      this._index = this._length;
    } else {
      this._index = value;
    }
  }

  indexOf(searchval) {
    let fromindex = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
    //searchval must be Uint8array
    let index = -1;

    for (let i = fromindex; i < this._length - searchval.length; i++) {
      let found = true;

      for (let j = 0; j < searchval.length; j++) {
        if (this._buffer[i + j] !== searchval[j]) {
          found = false;
          break;
        }
      }

      if (found) {
        index = i;
        break;
      }
    }

    return index;
  }

  jump_to(searchval) {
    //TODO return the length of the jump
    const foundval = this.indexOf(searchval, this._index);
    this.set_index(foundval + searchval.length);
    return foundval;
  }

  get_length() {
    return this._length;
  }

  get_remaining() {
    return this._buffer.subarray(this._index, this._length);
  }

  read_func(meth, len) {
    let res = meth(this._buffer, this._index);
    this.set_index(this._index + len);
    return res;
  }

  read() {
    let count = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

    const result = this._buffer.subarray(this._index, this._index + count);

    this.set_index(this._index + count);
    return result;
  }

  read_to(separator) {
    const index = this.indexOf(separator, this._index);

    const result = this._buffer.subarray(this._index, index);

    this.set_index(index + separator.length);
    return result;
  }

  read_encoding(format_string) {
    let data = Array(format_string.length);

    for (var i = 0; i < format_string.length; i++) {
      data[i] = this.decode_method[format_string.charAt(i)]();
    }

    return data;
  }

}

exports.BinStream = BinStream;

function dictZip(key_array, val_array) {
  if (key_array.length === val_array.length) {
    return key_array.reduce((acc, curr, index) => {
      acc[curr] = val_array[index];
      return acc;
    }, {});
  } else {
    console.error("Wrong length");
  }
}

;

class LogField {
  constructor(name, encoding, labels) {
    this.name = name;
    this.encoding = encoding;
    this.labels = labels.split(',');
    this.units = [];
    this.mults = [];
  }

  applymults(data) {
    const newdata = [];

    for (let i = 0; i < data.length; i++) {
      if (this.mults[i] === 0) {
        newdata.push(data[i]);
      } else {
        newdata.push(data[i] * this.mults[i]);
      }
    }

    return newdata;
  }

  makedict(data) {
    return dictZip(this.labels, data);
  }

}

exports.LogField = LogField;
const separator = new Uint8Array([163, 149]);
let reject_count = 0;

function applyChar(char_vals, lookup) {
  let target = [];

  for (let i = 0; i < char_vals.length; i++) {
    target.push(lookup[char_vals.charCodeAt(i)]);
  }

  return target;
}

class Bin {
  constructor(buf) {
    this._stream = new BinStream(buf);
    this._fields = {
      128: new LogField('FMT', 'BBnNZ', 'Type,Length,Name,Format,Columns')
    };
    this._fields[128].mults = [0, 0, 0, 0]; //this._read_field.bind(this);

    this._field_handlers = {};
    this._units = {};
    this._mults = {};
    this._parms = {};
    this._data = {};

    for (let i = 0; i < 500; i++) {
      this._field_handlers[i] = this._read_data.bind(this);
    }

    this._field_handlers[128] = this._create_field.bind(this); // FMT

    this._special_fields = {
      'FMT': this._create_field.bind(this),
      'UNIT': this._add_units.bind(this),
      'MULT': this._add_mults.bind(this),
      'PARM': this._read_parm.bind(this),
      'FMTU': this._read_fmtu.bind(this)
    };
    this.parse_all.bind(this);
    this.get_parms.bind(this);
    this.get_data.bind(this);
    this.get_fields.bind(this);
    this.get_mults.bind(this);
    this.get_units.bind(this);
  }

  get_parms() {
    return this._parms;
  }

  get_data() {
    return this._data;
  }

  get_fields() {
    return this._fields;
  }

  get_mults() {
    return this._mults;
  }

  get_units() {
    return this._units;
  }

  parse_all() {
    while (!this._stream._eof) {
      this._parse_line();
    }

    console.log(reject_count);

    for (let i = 0; i < Object.keys(this._data).length; i++) {
      if (this._data[Object.keys(this._data)[i]].length === 0) {
        //delete the dictionaries that dont have data
        delete this._data[Object.keys(this._data)[i]];
      }
    }

    for (let fieldid = 0; fieldid < Object.keys(this._fields).length; fieldid++) {
      let field = this._fields[fieldid];

      if (field) {
        if (this._data[field.name]) {
          for (let i = 0; i < this._data[field.name].length; i++) {
            this._data[field.name][i] = field.applymults(this._data[field.name][i]);
          }
        }
      }
    }
  }

  _parse_line() {
    this._stream.jump_to(separator);

    const id = this._stream.decode_method['B']();

    try {
      const data = this._read_field(this._fields[id]);

      this._field_handlers[id](data, id);

      return data;
    } catch (err) {
      reject_count++;
    }
  }

  _read_field(field) {
    return this._stream.read_encoding(field.encoding);
  }

  _create_field(data, fieldId) {
    const fmt = this._fields[fieldId].makedict(data);

    if (!(fmt.Type in this._fields)) {
      this._fields[fmt.Type] = new LogField(fmt.Name, fmt.Format, fmt.Columns); //console.log(fmt.Name);
      //console.log(Object.keys(this._special_fields));

      if (fmt.Name in this._special_fields) {
        this._field_handlers[fmt.Type] = this._special_fields[fmt.Name];
      } else {
        this._data[fmt.Name] = [];
      }
    }
  }

  _add_units(data, fieldId) {
    const unit = this._fields[fieldId].makedict(data);

    this._units[unit.Id] = unit.Label;
  }

  _add_mults(data, fieldId) {
    const mult = this._fields[fieldId].makedict(data);

    this._mults[mult.Id] = mult.Mult;
  }

  _read_parm(data, fieldId) {
    const parm = this._fields[fieldId].makedict(data);

    this._parms[parm.Name] = parm.Value;
  }

  _read_fmtu(data, fieldId) {
    const fmtu = this._fields[fieldId].makedict(data);

    this._fields[fmtu.FmtType].units = applyChar(fmtu.UnitIds, this._units);
    this._fields[fmtu.FmtType].mults = applyChar(fmtu.MultIds, this._mults);
  }

  _read_data(data, fieldId) {
    this._data[this._fields[fieldId].name].push(data); // this will read a generic field and store the data
    // mults still need to be applied

  }

}

exports.Bin = Bin;